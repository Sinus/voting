#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "err.h"

int main(int argc, char* argv[])
{
    int x, fdR, fdW;

    if (mkfifo("./.s_read", 0600) == -1)
        syserr("Error in mkfifo");

    if (mkfifo("./.s_write", 0600) == -1)
        syserr("Error in mkfifo");
    printf("zrobilem fifo\n");
    x = 1;

    /*if (fdW = open("./.s_write", 0600) == -1)
        syserr("err");
    if (fdR = open("./.s_read", 0600) == -1)
        syserr("errr");*/
    fdW = open("./.s_write", O_WRONLY);
    fdR = open("./.s_read", O_RDONLY);

    printf("otworzylem fifo\n");

    if (fdW == -1 || fdR == -1)
        syserr("Error in open");

    if (write(fdW, &x, sizeof(x)) == -1)
        syserr("error in write");

    printf("napisalem\n");

    if (unlink("./.s_write") == -1)
        syserr("Error in unlink");
    if (unlink("./.s_read") == -1)
        syserr("Error in unlink");
    return 0;
}
