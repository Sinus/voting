ALL: err.h err.c serwer.c mesg.h komisja.c raport.c
	gcc err.h err.c mesg.h serwer.c -lpthread -o serwer
	gcc err.h err.c mesg.h komisja.c -Wall -o komisja
	gcc err.h err.c mesg.h raport.c -Wall -o raport

clean:
	rm serwer komisja raport err.h.gch mesg.g.gch -f
