#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <sys/ipc.h>
#include <sys/msg.h>

#include "err.h"
#include "mesg.h"

#define READ_QUEUE 222
#define WRITE_QUEUE 111

long m;
const long maxM = 10000;

int main(int argc, char* argv[])
{
    int i, j, msgInQ, msgOutQ, n, allowed, voted;
    Mesg mesg;
    pid_t pid;
    pid = getpid();

    if (argc != 2)
        fatal("Wrong number of arguments");
    
    m = (long)atoi(argv[1]);

    scanf("%d %d", &i, &j);
    allowed = i;
    voted = j;

    mesg.mesgType = 1L;
    mesg.val1 = m + 1;
    mesg.val2 = i;
    mesg.val3 = j;
    mesg.val4 = pid;

    if ((msgInQ = msgget(READ_QUEUE, 0)) == -1)
        syserr("msgget");

    if ((msgOutQ = msgget(WRITE_QUEUE, 0)) == -1)
        syserr("msgget");

    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    if (msgrcv(msgInQ, &mesg, sizeof(Mesg), pid, 0) < 0)
        syserr("msgrcv");

    if (mesg.val1 == 0)
    {
        printf("Odmowa dostepu\n");
        return 0;
    }    

    while (scanf("%d %d %d", &i, &j, &n) == 3)
    {
        mesg.mesgType = pid;
        mesg.val1 = i;
        mesg.val2 = j;
        mesg.val3 = n;
        mesg.val4 = pid;

        if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
            syserr("msgsnd");
    }

    mesg.mesgType = pid;
    mesg.val1 = -1;
    mesg.val2 = -1;
    mesg.val3 = -1;
    mesg.val4 = pid;

    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    if (msgrcv(msgInQ, &mesg, sizeof(Mesg), pid, 0) < 0)
        syserr("msgrcv");

    printf("Przetworzonych wpisow: %d\n", mesg.val1);
    printf("Uprawnionych do glosowania: %d\n", allowed);
    printf("Glosow waznych: %d\n", mesg.val2);
    printf("Glosow niewaznych: %d\n", voted - mesg.val2);
    double attendance = (voted * 100) / allowed;
    printf("Frekwencja w lokalu: %f\n", attendance);

    return 0;
}
