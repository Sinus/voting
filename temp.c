#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "mesg.h"
#include "err.h"

int main(int argc, char* argv[])
{
    int id;
    Mesg msg;

    if ((id = msgget(1, 0)) == -1)
        syserr("msgget");

    printf("id = %d\n", id);

    msg.mesgType = 1;
    msg.val1 = 2;
    msg.val2 = 3;
    msg.val3 = 5;

    if (msgsnd(id, (char *) &msg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    return 0;
}
