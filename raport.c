#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ipc.h>
#include <sys/msg.h>

#include "err.h"
#include "mesg.h"

#define WRITE_QUEUE 111
#define READ_QUEUE 222 

int main(int argc, char* argv[])
{
    int l, msgOutQ, msgInQ;
    Mesg mesg;
    pid_t pid;
    int ok, bad, allowed;

    pid = getpid();

    if (argc > 2)
        fatal("Wrong number of arguments");

    if (argc == 2)
        l = atoi(argv[1]);
    else
        l = 0;

    if ((msgInQ = msgget(READ_QUEUE, 0)) == -1)
        syserr("msgget");

    if ((msgOutQ = msgget(WRITE_QUEUE, 0)) == -1)
        syserr("msgget");

    mesg.mesgType = 1L;
    mesg.val1 = -1;
    mesg.val2 = l;
    mesg.val4 = pid;

    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) < 0)
        syserr("msgsnd");

    if (msgrcv(msgInQ, &mesg, sizeof(Mesg), pid, 0) < 0)
        syserr("msgrcv");

    ok = mesg.val4;
    allowed = mesg.val3;
    printf("Przetworzonych komisji: %d / %d\n", mesg.val1, mesg.val2);
    printf("Uprawnionych do glosowania: %d\n", allowed);
    printf("Glosow waznych: %d\n", ok);

    if (msgrcv(msgInQ, &mesg, sizeof(Mesg), pid, 0) < 0)
        syserr("msgrcv");

    bad = mesg.val1;
    printf("Glosow niewaznych: %d\n", bad);


    double attendance;
    if (allowed > 0)
        attendance = ((ok + bad) * 100) / allowed;
    if (allowed > 0)
        printf("Frekwencja: %f%%\n", attendance);
    else
        printf("Frekwencja: 100%%\n");


    while (1)
    {
        if (msgrcv(msgInQ, &mesg, sizeof(Mesg), pid, 0) < 0)
            syserr("msgrcv");

        if (mesg.val1 != -1)
            printf("Lista %d kandydat %d: %d\n", mesg.val1, mesg.val2,
                    mesg.val3);
        else
            break;
    }

    return 0;
}
