#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>

#include "err.h"
#include "mesg.h"

#define READ_QUEUE 111
#define WRITE_QUEUE 222

int l, k, m, msgInQ, msgOutQ, noDone = 0;
pthread_mutex_t mutex, lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t report = PTHREAD_COND_INITIALIZER;
int** votes;
int entitiledToVote = 0, allVotes = 0, okVotes = 0, activeThreads = 0;
short raportWaiting = 0;
short * done;
short * isAlive;
pthread_t * threadArray;
pthread_t reportThread;
int waiting = -1;
int reportsAlive = 0;

void *killThread(int clientPid)
{
    if (kill(clientPid, SIGINT) != 0)
        syserr("kill");
}

void *handleRaport(void *p)
{
    sigset_t set;
    sigemptyset(&set); /*block signals*/
    sigaddset(&set, SIGINT);
    if (pthread_sigmask(SIG_BLOCK, &set, NULL) != 0)
        syserr("pthread_sigmask");
    Mesg mesg;
    int dest, type, i, j;

    dest = ((Mesg*)p)->val4;
    mesg.mesgType = dest;
    type = ((Mesg*)p)->val2;

    /*send initial data (attendance etc.)*/
    mesg.val1 = noDone;
    mesg.val2 = k;
    mesg.val3 = entitiledToVote;
    mesg.val4 = okVotes;

    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    mesg.mesgType = dest;
    mesg.val1 = allVotes - okVotes;
    mesg.val2 = -1;
    mesg.val3 = -1;
    mesg.val4 = dest;

    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    pthread_mutex_lock(&mutex);
    if (type == 0) /*report evrything*/
    {
        for (i = 1; i <= l; i++)
        {
            for (j = 1; j <= k; j++)
            {
                mesg.mesgType = dest;
                mesg.val1 = i;
                mesg.val2 = j;
                mesg.val3 = votes[i][j];
                mesg.val4 = dest;
                if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
                    syserr("msngsnd");
            }
        }
    }
    else /*report only mesg.val2 list*/
    {
        for (i = 1; i <= k; i++)
        {
            mesg.mesgType = dest;
            mesg.val1 = type;
            mesg.val2 = i;
            mesg.val3 = votes[type][i];
            mesg.val4 = dest;
            if (msgsnd(msgOutQ, (char *) &mesg, sizeof (Mesg), 0) != 0)
                syserr("msgsnd");
        }
    }
    pthread_mutex_unlock(&mutex);
    /*end of report*/
    mesg.val1 = -1;
    mesg.val2 = -1;
    mesg.val3 = -1;
    mesg.val4 = dest;
    if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
        syserr("msgsnd");

    pthread_mutex_lock(&mutex);
    reportsAlive--;
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&report);
}

void *handleCustomer(void *p)
{
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT); /*block signals*/
    if (pthread_sigmask(SIG_BLOCK, &set, NULL) != 0)
        syserr("pthread_sigmask");
    pthread_cleanup_push(killThread, ((Mesg*)p)->val4); /*cleanup block will
                                                          kill client if sigint
                                                          is captured*/
    int voteDone = 0, client, okVotesHere = 0;
    Mesg mesg;
    int temp;

    client = ((Mesg*)p)->val4;

    int num = ((Mesg*)p)->val1;
    temp = num; /*wthout this num gets changed after msgrcv*/
    while (1)
    {        
        if (msgrcv(msgInQ, &mesg, sizeof(Mesg), client, 0) < 0)
            syserr("mesgrcv");

        if (mesg.val1 == -1) /*end of communication*/
        {
            pthread_mutex_lock(&mutex);
            mesg.mesgType = mesg.val4;
            mesg.val1 = voteDone;
            mesg.val2 = okVotesHere;
            noDone++;
            pthread_mutex_unlock(&mutex);
            mesg.val3 = 0;
            if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
                syserr("msgsnd");
            pthread_mutex_lock(&mutex);
            pthread_mutex_lock(&lock);
            activeThreads--;
            pthread_mutex_unlock(&mutex);
            pthread_cond_signal(&report);
            pthread_mutex_unlock(&lock);
            pthread_mutex_lock(&mutex);
            isAlive[num] = 0;
            pthread_mutex_unlock(&mutex);
            break;
        }
        else /*read votes*/
        {
            pthread_mutex_lock(&mutex);
            voteDone += mesg.val3;
            if (mesg.val1 > 0 && mesg.val1 <= l && mesg.val2 > 0 &&
                    mesg.val2 <= k)
            {
                votes[mesg.val1][mesg.val2] += mesg.val3;
                okVotes += mesg.val3;
                okVotesHere += mesg.val3;
            }
            pthread_mutex_unlock(&mutex);
        }
    }
    pthread_cleanup_pop(0);
}

static void sigHandler(int signum)
{
    int i;
    Mesg mesg;

    if (waiting != -1) /*kill waiting client*/
        if (kill(waiting, SIGINT) != 0)
            syserr("kill");

    pthread_mutex_lock(&mutex);
    for (i = 0; i <= m; i++)
    {
        /*kill all threads reading votes*/
        if (isAlive[i] == 1)
        {
            if (pthread_cancel(threadArray[i]) != 0)
                syserr("pthread_cancel");
            if (pthread_join(threadArray[i], NULL) != 0)
                syserr("pthread_join");
        }
    }
    pthread_mutex_unlock(&mutex);

    /*wait untill reports are done*/
    pthread_mutex_lock(&lock);
    while (reportsAlive != 0)
        pthread_cond_wait(&report, &lock);

    while (1)
    {
        /*send sigint to all remaining clients*/
        if (msgrcv(msgInQ, &mesg, sizeof(Mesg), 0, IPC_NOWAIT) < 0)
        {
            if (errno == ENOMSG)
                break;
            else
                syserr("mesgrcv");
        }
        if (kill(mesg.val4, SIGINT) != 0)
            syserr("kill");
    }

    /*free resources*/
    if (pthread_mutex_destroy(&mutex) < 0)
        syserr("pthread_mutex_destroy");
    if (pthread_mutex_destroy(&lock) < 0)
        syserr("pthread_mutex_destroy");

    free(done);
    free(isAlive);
    free(threadArray);

    for (i = 0; i <= m; i++)
        free(votes[i]);
    free(votes);

    if (msgctl(msgInQ, IPC_RMID, NULL) == -1)
        syserr("msgctl");

    if (msgctl(msgOutQ, IPC_RMID, NULL) == -1)
        syserr("msgctl");

    exit(0);
}

int main(int argc, char* argv[])
{
    int error, i, n, temp;
    Mesg mesg;
    struct sigaction sa;

    sa.sa_handler = sigHandler;
    sigemptyset(&sa.sa_mask); /*handle sigint*/
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGINT, &sa, NULL) == -1)
        syserr("sigaction");

    /*initialize resources*/
    if (argc != 4)
        fatal("Za malo argumentow!");

    l = atoi(argv[1]);
    k = atoi(argv[2]);
    m = atoi(argv[3]);

    votes = (int**)malloc((l + 1) * sizeof(int*));
    for (i = 1; i <= l; i++)
        votes[i] = (int*)malloc((k + 1) * sizeof(int));

    if (pthread_mutex_init(&mutex, NULL) != 0)
        syserr("mutex_init");

    threadArray = malloc(sizeof(pthread_t) * m + 1);

    done = malloc(sizeof(short) * m + 1);
    for (i = 0; i <= m; i++)
        done[i] = 0;

    isAlive = malloc (sizeof(short) * m + 1);
    for (i = 0; i <= m; i++)
        isAlive[i] = 0;

    if ((msgInQ = msgget(READ_QUEUE, 0600 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr("msgget");

    if ((msgOutQ = msgget(WRITE_QUEUE, 0600 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr("msgget");

    /*create threads for clients*/
    while (1)
    {
        if ((n = msgrcv(msgInQ, &mesg, sizeof(Mesg), 1L, 0)) < 0)
            syserr("msgrcv");
        if (mesg.val1 == -1) /*report*/
        {
            waiting = mesg.val4;
            pthread_mutex_lock(&lock);
            while (activeThreads > 0)
                pthread_cond_wait(&report, &lock);
            pthread_mutex_lock(&mutex);
            reportsAlive++;
            waiting = -1;
            pthread_mutex_unlock(&mutex);
            if (pthread_create(&reportThread, NULL, &handleRaport, &mesg) != 0)
                syserr("create");
            pthread_mutex_unlock(&lock);
        }

        else /*votes*/
        {
            if (done[mesg.val1] == 0) /*new commision, read it's votes*/
            {
                pthread_mutex_lock(&mutex);
                pthread_mutex_lock(&lock);
                activeThreads++;
                pthread_mutex_unlock(&lock);
                done[mesg.val1] = 1;
                temp = mesg.val1;
                entitiledToVote += mesg.val2;
                allVotes += mesg.val3;
                pthread_mutex_unlock(&mutex);
                mesg.mesgType = mesg.val4;
                if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
                    syserr("msgsnd");
                if ((error = pthread_create(&threadArray[mesg.val1], NULL,
                                &handleCustomer, &mesg)) != 0)
                    syserr("create");
                isAlive[temp] = 1;
            }
            else /*we allready did this one, deny access*/
            {
                mesg.mesgType=mesg.val4;
                mesg.val1 = 0;

                if (msgsnd(msgOutQ, (char *) &mesg, sizeof(Mesg), 0) != 0)
                    syserr("msgsnd");
            }
        }
    }
    return 0;
}
